CREATE TABLE _Articles_old_20160118
(
    id INTEGER PRIMARY KEY NOT NULL,
    title TEXT,
    body TEXT,
    created_date INTEGER
);
CREATE TABLE _Articles_old_20160118_1
(
    id INTEGER PRIMARY KEY NOT NULL,
    title TEXT,
    body TEXT,
    created_at INTEGER
);
CREATE TABLE article_tag
(
    article_id INTEGER PRIMARY KEY NOT NULL,
    tag_id INTEGER NOT NULL,
    created_at TEXT NOT NULL,
    updated_at TEXT NOT NULL,
    FOREIGN KEY (article_id) REFERENCES articles (id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
);
CREATE UNIQUE INDEX article_tag_tag_id_index ON article_tag (tag_id);
CREATE UNIQUE INDEX article_tag_article_id_index ON article_tag (article_id);
CREATE TABLE articles
(
    id INTEGER PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL,
    title TEXT,
    body TEXT,
    published_at TEXT NOT NULL,
    excerpt TEXT,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE migrations
(
    migration TEXT NOT NULL,
    batch INTEGER NOT NULL
);
CREATE TABLE password_resets
(
    email TEXT NOT NULL,
    token TEXT NOT NULL,
    created_at TEXT NOT NULL
);
CREATE UNIQUE INDEX password_resets_token_index ON password_resets (token);
CREATE UNIQUE INDEX password_resets_email_index ON password_resets (email);
CREATE TABLE sqlite_sequence
(
    name TEXT,
    seq TEXT
);
CREATE TABLE tags
(
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    created_at TEXT NOT NULL,
    updated_at TEXT NOT NULL
);
CREATE TABLE users
(
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    remember_token TEXT,
    created_at TEXT NOT NULL,
    updated_at TEXT NOT NULL
);
CREATE UNIQUE INDEX users_email_unique ON users (email);