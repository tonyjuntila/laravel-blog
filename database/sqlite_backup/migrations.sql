INSERT INTO migrations (migration, batch) VALUES ('2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations (migration, batch) VALUES ('2014_10_12_100000_create_password_resets_table', 2);
INSERT INTO migrations (migration, batch) VALUES ('2016_01_05_124423_create_articles_table', 2);
INSERT INTO migrations (migration, batch) VALUES ('2016_01_13_114244_add_excerpt_to_articles_table', 2);
INSERT INTO migrations (migration, batch) VALUES ('2017_04_12_120915_create_tags_table', 2);
INSERT INTO migrations (migration, batch) VALUES ('2017_04_12_124342_create_users_table', 3);