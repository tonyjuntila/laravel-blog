<!DOCTYPE html>
<html>
<head>
    <title>Laravel Fundamentals</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{ elixir('css/all.css') }}" rel='stylesheet'>
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>

    @include('partials.nav')

    <div class="container">

        {{--using laracasts service provider--}}
        {{--@include('flash::message')--}}

        @include('partials.flash')

        @yield('content')

    </div>

    <div style="position: absolute; margin-top: 100%; margin-left: -50%;" >
        <script src="{{ elixir('js/all.js') }}"></script>
        <script>
            $('div.alert').not('.alert-important').delay(3000).slideUp(300);
        </script>

        @yield('footer')


    </div>


</body>
</html>
