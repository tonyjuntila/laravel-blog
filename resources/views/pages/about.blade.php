@extends('app')

@section('content')
    <div class="content">
        {{--<div class="title">About Page: {!! $first !!} {!! $last !!}</div>--}}

        <ul>
            @foreach($people as $person)
                <li>{{ $person }}</li>
            @endforeach

        </ul>
    </div>
@stop

@section('footer')
    copyright
@stop