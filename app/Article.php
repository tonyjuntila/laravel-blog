<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'body',
        'published_at'
    ];

    protected $dates = ['published_at'];

    public  function scopePublished($query)
    {
        $query->where('published_at','<=',Carbon::now());
    }

    public  function scopeUnpublished($query)
    {
        $query->where('published_at','>',Carbon::now());
    }

    public  function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    /**
     * Get the published_at attribute
     * @param $date
     * @return string
     */
    public function getPublishedAttribute($date)
    {
//        return new Carbon($date);
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User'); //$article->user
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    /**
     * GEt a list of tag ids associated with the current article.
     * @return array
     */

    public function getTagListAllAttribute()
    {
        return $this->tags->lists('id');
    }
}
