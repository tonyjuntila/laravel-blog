<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests as appRequest;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Request as RequestFacade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use Auth;
use App\Tag;


class ArticlesController extends Controller
{
    /**
     * create a new articles controller instance.
     * ArticlesController constructor.
     */
    public function __construct()
    {
/*        reffering in Kernel.php routeMiddleware*/
//          $this->middleware('auth');
//          $this->middleware('auth',['only' => 'create']);
        $this->middleware('auth',['except'=>'index','show']);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

//        $articles = Article::all();

//        $articles = Article::latest()->get();

//        $articles = Article::latest('published_at')->where('published_at','<=',Carbon::now()->get());

        $articles = Article::latest('published_at')->published()->get();

//        $latest = Article::latest()->first();

        return view('articles.index', compact('articles'));
        //return $articles;
        //return "get all articles";

    }

//    public function show($id){
//        dd($id);
//        $article = Article::findOrFail($id);

//        dd($article->created_at->addDays(8)->diffForHumans());
//        dd($article->published_at);
    /*
     *
     *@param Article $id
     * */
    /**
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Article $article){
        return view('articles.show', compact('article'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
//        if(Auth::guest()){
//            return redirect('articles');
//        }

        $tags = Tag::lists('name', 'id');

        return view('articles.create', compact('tags'));
    }

    /**
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request){

//        $input = RequestFacade::all();
//        $input['created_at'] = Carbon::now();
//        $input['updated_at'] = Carbon::now();
//        Article::create($input);

//        Article::create(RequestFacade::all());

//        $this->validate($request, ['title' => 'required', 'body' => 'required']);
//
//        Article::create($request->all());
        
        /*Using Auth*/

//        $article = new Article($request->all());
//
//        Auth::user()->articles()->save($article);

//        \Session::flash('flash_message','Your article has been created');

//        session()->flash('flash_message','Your article has been created');
//        session()->flash('flash_message_important', true);

          /*Using Laracast Service Provider*/
//        flash('Your article has been created');
//        return redirect('articles');

//        return redirect('articles')->with([
//            'flash_message' => 'flash_message','Your article has been created',
//            'flash_message_important' => true
//        ]);

//        dd($request->input('tags'));
//        Auth::user()->articles()->create($request->all());
//        dd($request->all());

//        $article->tags()->attach($request->input('tag_list'));

/*        $article = Auth::user()->articles()->create($request->all());
        $this->syncTags($article, $request->input('tag_list'));*/

        $this->createArticle($request);

        session()->flash('flash_message','Your article has been created');

        return redirect('articles');

    }

//    public function edit($id){
//    $article = Article::findOrFail($id);

    /**
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Article $article){

        $tags = Tag::lists('name', 'id');

        return view('articles.edit', compact('article','tags'));
    }

//    public function update($id, ArticleRequest $request){ //articles/{id}
//        $article = Article::findOrFail($id);

    /**
     * @param Article $article
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ArticleRequest $request, Article $article){ //articles/{id}
        
        $article->update($request->all());

//        $article->tags()->sync($request->input('tag_list'));

        $this->syncTags($article, $request->input('tag_list'));

        return redirect("articles");

    }

    /**
     * @param Article $article
     * @param array $tags
     */
    private function syncTags(Article $article, array $tags)
    {
        $article->tags()->sync($tags);
    }

    /**
     * Save a new article
     * @param ArticleRequest $request
     * @return mixed
     */
    private function createArticle(ArticleRequest $request)
    {
        $article = Auth::user()->articles()->create($request->all());

        $this->syncTags($article, $request->input('tag_list'));

        return $article;
    }
}
