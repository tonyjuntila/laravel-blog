<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{

    public function about()
    {
      //First approach
        //$name = "Tony";
//        $name = "<span style='color:red;'>Tony</span>"; //views like this {!! $first !!}
//        return view("pages.about")->with('first', $name);

        //Second approach
/*        return view("pages.about")->with([
            "first" => "Tony",
            "last" => "Stark",
        ]);*/

        //Third approach
/*        $data = [];
        $data['first'] = "Tony";
        $data['last'] = "Stark";
        return view("pages.about", $data);*/

/*        $first = "Tony";
        $last = "Stark";
        return view("pages.about", compact('first','last'));*/

        $people = [
            "tony stark", "robert downy", "iron man"
        ];
        return view("pages.about", compact('people'));

    }

    public function contact()
    {
        return view("pages.contact");

    }
}
