<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*Class Baz{}

Class Bar {

    public $baz;

    public function __construct(Baz $baz)
    {
        $this->baz = $baz;
    }
}

App::bind('Bar', function()
{
    return new Bar(new Baz);
});

Route::get('bar', function(Bar $bar){
   dd($bar);
});*/

/*interface BarInterface {}

class Bar implements BarInterface {}

app()->bind('BarInterface','Bar');

Route::get('bar', function(){
//    $bar = app()->make('BarInterface');
//    $bar = app()['BarInterface'];
    $bar = app('BarInterface');
    dd($bar);
});*/

Route::get('foo', 'FooController@foo');

Route::get('/', function () {
    return view('welcome');
});

//views location: resources/views/contact
/*Route::get('contact', function () {
    return view('contact');
});*/

//views location: resources/views/pages/contact
Route::get('about', 'PagesController@about');
Route::get('contact', 'PagesController@contact');

/*using middleware*/
//Route::get('about', ['middleware' => 'auth', 'uses' => 'PagesController@about']);
/*Route::get('about', ['middleware' => 'auth', function(){
  return 'this page will only show if the user is signed in';
}]);*/

Route::get('tags/{tags}','TagsController@show');

//Route::get('articles', 'ArticlesController@index');
//Route::get('articles/create', 'ArticlesController@create');
//Route::get('articles/{id}', 'ArticlesController@show');
//Route::post('articles', 'ArticlesController@store');
//Route::post('articles/{id}/edit', 'ArticlesController@edit');
Route::resource('articles', 'ArticlesController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

//explicit on Laravel 5.2 so you need to copy paste into route group
/*Route::group(['middleware' => ['web']], function () {
    Route::resource('articles', 'ArticlesController');
});*/

/*Route::get('foo',['middleware' => ['auth', 'manager'], function(){
    return 'this page may only ne viewed by managers';
}]);*/


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
});

//Route::get('foo/{bar}', function(){
//
//});

