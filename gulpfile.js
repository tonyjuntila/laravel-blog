var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    //mix.sass('app.scss', 'resources/assets');

/*    mix.sass('app.scss');
    mix.coffee('module.coffee');

    mix.styles([
        'vendor/normalize.css',
        'app.css'
    ], 'public/output/final.css', 'public/css');

    mix.scripts([
        'vendor/jquery.js',
        'main.js',
        'coupon.js'
    ],'public/output/scripts.js','public/js');*/

    //mix.phpUnit().phpSpec();

/*    mix.styles([
        'vendor/normalize.css',
        'app.css'
    ], null, 'public/css');*/

    mix.styles([
        'app.css',
        'libs/bootstrap.min.css',
        'libs/select2.min.css'
    ]);

    mix.scripts([
        'libs/jquery.js',
        'libs/bootstrap.min.js',
        'libs/select2.min.js'
    ]);

    mix.version([
        'public/css/all.css',
        'public/js/all.js'
    ]);

});
